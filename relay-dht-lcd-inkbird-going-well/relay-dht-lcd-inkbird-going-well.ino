  #include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include "DHT.h"

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

const char* ssid = "Paranodal";
const char* password = "sss";
const char* ssid2 = "outside";
const char* password2 = "outsideoutside";

ESP8266WebServer server(80); //Server on port 80

LiquidCrystal_I2C lcd(0x3F, 16, 2);

int relayInputHOT = 12; // the input to the relay pin for hot end D6
int relayInputCOLD = 13; // the input to the relay pin for cold end D7

float target = 18.00;
float tolerance = 0.20;

#define DHTPIN 2     // Digital pin connected to the DHT sensor
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);

float h = dht.readHumidity();
float t = dht.readTemperature();
float f = dht.readTemperature(true);


//===============================================================
// This routine is executed when you open its IP in browser
//===============================================================
void handleRoot() {
 float t = dht.readTemperature();
 String page = " <!DOCTYPE html><html><body>";
 page += "<style>body{text-align:center;}</style>";
 page += "<h2>Temperature controller<h2>";
 page += "<h3>Current Temp</h3>" + String(t);
 page += "<h3>Current temperature target:</h3>" + String(target);
 page += "<form action='/action_page'>";
 page += "New temp target:<br>";
 page += "<input type='text' name='temp-target' value='20.00'>";
 page += "<br>";
 page += "Tolerance:<br>";
 page += "<input type='text' name='tolerance' value='0.50'>";
 page += "<br><br>";
 page += "<input type='submit' value='Submit'>";
 page += "</form>";
 page += "</body></html>";
 server.send(200, "text/html", page); //Send web page
}
//===============================================================
// This routine is executed when you press submit
//===============================================================
void handleForm() {
 target = server.arg("temp-target").toFloat(); 
 tolerance = server.arg("tolerance").toFloat(); 
 String s = "<a href='/'> Go Back </a>";
 server.send(200, "text/html", s); //Send web page
}

void setup() {
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Homemade Tmp H%");
  lcd.setCursor(0, 1);
  lcd.print("Initialising....");

  pinMode(relayInputHOT, OUTPUT); // initialize pin as OUTPUT
  pinMode(relayInputCOLD, OUTPUT); // initialize pin as OUTPUT
  Serial.begin(9600);
  Serial.println(F("DHTxx test!"));
  dht.begin();

  int n = WiFi.scanNetworks();
  for (int i = 0; i < n; ++i) {
    if (WiFi.SSID(i)== ssid ) {
    WiFi.begin(ssid,password); //trying to connect the modem
    break;
    }
    if (WiFi.SSID(i)== ssid2) {
    WiFi.begin(ssid2,password2); //trying to connect the modem
    break;
    }
  }
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  //If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(WiFi.SSID()+" Wifi");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //IP address assigned to your ESP
 
  server.on("/", handleRoot);      //Which routine to handle at root location
  server.on("/action_page", handleForm); //form action is handled here

  server.begin();                  //Start server
  Serial.println("HTTP server started");

}

void loop() {

  delay(3000);

  float h = dht.readHumidity();
  float t = dht.readTemperature();
  float f = dht.readTemperature(true);
  
  server.handleClient();          //Handle client requests

  String screenInfo = "T:" + String(t) + " H%:" + String(h);

  lcd.setCursor(0, 0);
  lcd.print(screenInfo);
  lcd.setCursor(0, 1);
  lcd.print("Target: " + String(target));

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  if (t > (target - tolerance)){
    digitalWrite(relayInputHOT, LOW);
    Serial.print("Temperature low, turning on hot end");
  }
  if (t < (target + tolerance)){
    digitalWrite(relayInputHOT, HIGH);
    Serial.print("Temperature is high, turning off hot end");
  }

  float hic = dht.computeHeatIndex(t, h, false);

  delay(3000);

  lcd.setCursor(0, 0);
  lcd.print(WiFi.localIP());
  lcd.setCursor(0, 1);
  lcd.print("n:"+WiFi.SSID());
  
  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));
  Serial.print(F(" Humiture: "));
  Serial.print(hic);
  Serial.print(F("°C"));
  Serial.println();

}
