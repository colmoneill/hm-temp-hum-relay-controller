from api_keys import key
import requests
from requests.auth import HTTPBasicAuth
import json

# follow instructions in 'sample_api_keys' file to make a local only file containing auth credentials
auth = HTTPBasicAuth(key['userID'], key['pw'])

# response = requests.get('https://api.brewfather.app/v1/batches/' + batchId, auth=auth, params=query)

query = {'include' : ''}

brewfather_batches = "https://api.brewfather.app/v2/batches/"
response = requests.get(brewfather_batches, auth=auth, params=query)
batches = response.json()
#print(json.dumps(batches, indent=2) )
batches_fermenting = []
for batch in batches:
    if batch['status'] == 'Fermenting':
        #print(batch['_id'] + ' is ' + batch['status'] + ' called: ' + batch['recipe']['name'])
        batches_fermenting += [batch['_id']]

for batch_id in batches_fermenting:
    batch_query = 'https://api.brewfather.app/v2/batches/' + batch_id + '/readings/last'
    response = requests.get(batch_query, auth=auth, params=query)
    batch = response.json()
    print(json.dumps(batch, indent=2))
    #print(json.dumps(batch["devices"], indent=2))